
using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;


namespace NuclearWessels.MSBuild.Utils
{
    /// <summary>
    /// Change the FileVersion and ProductVersion tags in a Resource.rc file.  (Native builds.)
    /// </summary>
	public class ResourceRCVersion : Task	
	{
		[Required]
		public string ResourceFile
		{
			get; set;
		}


        // Can use a Version string to set the Major, Minor, Build and Private numbers.
        // Use an 'x' for values that need to remain unmodified.
        public string Version
        {
            get
            {
                return String.Format("{0}.{1}.{2}.{3}",
                    Major.HasValue ? Major.Value.ToString() : "x",
                    Minor.HasValue ? Minor.Value.ToString() : "x",
                    Build.HasValue ? Build.Value.ToString() : "x",
                    Private.HasValue ? Private.Value.ToString() : "x");
            }

            set
            {
                string[] parts = value.Split('.');

                if (parts.Length >= 1)
                {
                    if (String.Compare(parts[0], "x", true) == 0)
                        Major = null;
                    else
                        Major = int.Parse(parts[0]);
                }

                if (parts.Length >= 2)
                {
                    if (String.Compare(parts[1], "x", true) == 0)
                        Minor = null;
                    else
                        Minor = int.Parse(parts[1]);
                }

                if (parts.Length >= 3)
                {
                    if (String.Compare(parts[2], "x", true) == 0)
                        Build = null;
                    else
                        Build = int.Parse(parts[2]);
                }

                if (parts.Length == 4)
                {
                    if (String.Compare(parts[3], "x", true) == 0)
                        Private = null;
                    else
                        Private = int.Parse(parts[3]);
                }

            }
        }


        public int? Major
        {
            get; set;
        }

        public int? Minor
        {
            get; set;
        }

        public int? Build
        {
            get; set;
        }

        public int? Private
        {
            get; set;
        }
		

		public override bool Execute ()
		{
            string[] lines = File.ReadAllLines(ResourceFile);

            string replace1String = String.Format(@"$1 {0},{1},{2},{3}",
                Major.HasValue ? Major.Value.ToString() : "$3",
                Minor.HasValue ? Minor.Value.ToString() : "$4", 
                Build.HasValue ? Build.Value.ToString() : "$5",
                Private.HasValue ? Private.Value.ToString() : "$6");

            string replace2String = String.Format("$1 \"{0}.{1}.{2}.{3}\"",
                Major.HasValue ? Major.Value.ToString() : "$3",
                Minor.HasValue ? Minor.Value.ToString() : "$4",
                Build.HasValue ? Build.Value.ToString() : "$5",
                Private.HasValue ? Private.Value.ToString() : "$6");

            Regex fileVersion1Regex = new Regex(@"(\s*FILEVERSION\s*)((\d+).(\d+).(\d+).(\d+))");
            Regex fileVersion2Regex = new Regex("([ ] * VALUE \"FileVersion\", )\"((\\d+).(\\d+).(\\d+).(\\d+))\"");

            Regex productVersion1Regex = new Regex(@"(\s*PRODUCTVERSION\s*)((\d+).(\d+).(\d+).(\d+))");
            Regex productVersion2Regex = new Regex("([ ] * VALUE \"ProductVersion\", )\"((\\d+).(\\d+).(\\d+).(\\d+))\"");

            string[] newLines = new string[lines.Length];
            for (int i=0; i<lines.Length; i++)
            {
                string line = lines[i];

                if (fileVersion1Regex.IsMatch(lines[i]))
                {
                    string result = fileVersion1Regex.Replace(lines[i], replace1String);
                    newLines[i] = result;
                }
                else if (productVersion1Regex.IsMatch(lines[i]))
                {
                    string result = productVersion1Regex.Replace(lines[i], replace1String);
                    newLines[i] = result;
                }
                else if (fileVersion2Regex.IsMatch(lines[i]))
                {
                    string result = fileVersion2Regex.Replace(lines[i], replace2String);
                    newLines[i] = result;
                }
                else if (productVersion2Regex.IsMatch(lines[i]))
                {
                    string result = productVersion2Regex.Replace(lines[i], replace2String);
                    newLines[i] = result;
                }
                else
                    newLines[i] = lines[i];
            }

            File.WriteAllLines(ResourceFile, newLines);

            return true;
		}


    }



}
